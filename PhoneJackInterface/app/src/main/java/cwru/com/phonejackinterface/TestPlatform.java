package cwru.com.phonejackinterface;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.UUID;

public class TestPlatform extends AppCompatActivity {

    public static String LOGTAG = "cwru.com.TestPlataform.java";
    private final static int POWER_BASE = 2;
    private final static int POWER_EXPONENT = 20;
    private static int BUFFER_SIZE_IN_BYTES = 1024;

    private Button _sendButton;
    private Button _RecordButton;
    private Button _stopRecordButton;
    private Button _talkButton;
    private RadioButton _textRB;
    private RadioButton _soundRB;
    private RadioGroup _sendRBgroup;
    private ProgressBar _talkProgress;
    private EditText _signalOut;
    protected TextView _MsgRcv;
    private RecordingClass rc;
    private static OutSignalService serv = null;
    private static byte[] inputData = new byte[BUFFER_SIZE_IN_BYTES];

    private final double _range = Math.pow(POWER_BASE, POWER_EXPONENT);

    private static TestPlatform instance = null;
    private int isPlugged; //0 means not plugged, 1 is plugged
    private int isMicIn;

    private BroadcastReceiver mReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_platform);
        IntentFilter filter = new IntentFilter(AudioManager.ACTION_HEADSET_PLUG);
        init();
        System.out.println("hi " + getApplicationContext());
        rc = new RecordingClass(TestPlatform.instance, 0);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();
                System.out.println("receive jack INTENT rcvd");
                if (AudioManager.ACTION_HEADSET_PLUG.equals(action)) {
                    isPlugged = intent.getIntExtra("state", -1);
                    isMicIn = intent.getIntExtra("microphone", -1);
                    Log.d("HeadSetPlugInTest", "state: " + isPlugged);
                    Log.d("HeadSetPlugInTest", "microphone: " + isMicIn);
                    if(isPlugged == 1) {
                        setTalkOption(false);
                        setSendOption(true);
                        System.out.println(" is plugged");
                        rc.makeAudioRecord(0);
                    }
                    else {
                        System.out.println(" is not plugged");
                        setTalkOption(true);
                        setSendOption(false);
                        rc.makeAudioRecord(1);
                    }
                }
            }
        };

        getApplicationContext().registerReceiver(mReceiver, filter);

    }

    public static TestPlatform getInstance(){
        return instance;
    }

    public void displayMssg(String mssg){
        Toast.makeText(getApplicationContext(), mssg,
                Toast.LENGTH_LONG).show();
    }

    private void init() {
        attachObjectsToVars();
        setEventListeners();
        startSignalServices();

        instance = this;
    }

    private void startSignalServices() {
        serv = new OutSignalService();
        serv.setUIActivity(this);
        //sendBroadcast(new Intent(this, TestReceiver.class));
    }

    private void attachObjectsToVars() {
        _sendButton         = (Button)findViewById(R.id.sendButton);
        _RecordButton       = (Button)findViewById(R.id.recordButton);
        _stopRecordButton   = (Button)findViewById(R.id.stopRecord);
        _talkButton         = (Button)findViewById(R.id.talkButton);
        _signalOut          = (EditText)findViewById(R.id.signalOut);
        _MsgRcv             = (TextView)findViewById(R.id.MsgRcv);
        _sendRBgroup        = (RadioGroup) findViewById(R.id.sendRBgroup);
        _talkProgress       = (ProgressBar) findViewById(R.id.TalkProgress);
        _talkProgress.setMax(100);
    }

    private void setEventListeners() {

        _sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int option = getRBindex();
                switch (option) {
                    case 0://audio
                        if(isPlugged == 1) {
                            System.out.println(rc.voiceBufUsed());
                            if(rc.voiceBufUsed()) {
                                rc.writeBufToAudio();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "no voice data recorded",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                        setTalkProgress(0);
                        break;
                    case 1://text
                        if (!isSendFieldEmpty()) {
                            outputSignal(_signalOut.getText().toString());
                        } else {
                            Toast.makeText(getApplicationContext(), "Not all data provided",
                                    Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }

            private boolean isSendFieldEmpty() {
                return _signalOut.getText().toString().isEmpty();
            }
        });

        _talkButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch( View yourButton , MotionEvent theMotion ) {
                switch ( theMotion.getAction() ) {
                    case MotionEvent.ACTION_DOWN:
                        rc.setTalk(true);
                        rc.startRecordingVoice();
                        break;
                    case MotionEvent.ACTION_UP:
                        rc.setTalk(false);
                        break;
                }
                return true;
            }
        });

        _RecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rc.startRecording();
            }
        });

        _stopRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rc.stopRecording();
            }
        });
    }

    public void setTalkOption(boolean isTalkOn){
        if (isTalkOn) {
            _talkButton.setAlpha(1f);
            _talkButton.setClickable(true);
        } else {
            _talkButton.setAlpha(.5f);
            _talkButton.setClickable(false);
        }
    }

    public void setSendOption(boolean isSendOn){
        if (isSendOn) {
            _sendButton.setAlpha(1f);
            _sendButton.setClickable(true);
        } else {
            _sendButton.setAlpha(.5f);
            _sendButton.setClickable(false);
        }
    }

    public int getRBindex(){
        return _sendRBgroup.indexOfChild(findViewById(_sendRBgroup.getCheckedRadioButtonId()));
    }

    public void setTalkProgress(int progress){
        _talkProgress.setProgress(progress);
    }


    private void outputSignal(String data) {
            inputData = hexStringToByteArray(data);
            if(inputData != null){
                start();
                Toast.makeText(getApplicationContext(), "Transmitting",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Data could not be encoded",
                        Toast.LENGTH_LONG).show();
            }
    }

    public void start() {
        if(isServiceRunning(OutSignalService.class.getName())){
          //serv.onDestroy(); //TODO: get on destroy for process. How to invoke?
        }
        String myId = UUID.randomUUID().toString();
        Log.d(LOGTAG, "Service going to be called: " + myId);
        Intent intent = new Intent(this, OutSignalService.class);
        intent.putExtra("ID", myId);
        intent.putExtra("inputData", inputData);
        startService(intent);
    }

    public boolean isServiceRunning(String serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static byte[] hexStringToByteArray(String str) {
        byte[] b = null;
        try{
            b = str.getBytes("US-ASCII");
        } catch (java.io.UnsupportedEncodingException e){

        }
        return b;
    }



    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test_platform, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
