package cwru.com.phonejackinterface;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;

import java.util.Arrays;

/**
 * Created by jimmy1 on 4/3/2016.
 */
public class SendAudio {
    int RECEIVE_SAMPLE = 48000;
    int SAMPLE_RATE_IN_HZ = 48000;
    private static int BUFFER_SIZE_IN_BYTES = 4096;
    Thread transmissionThread;
    boolean isRecording;
    int NUM_SAMPLES = AudioRecord.getMinBufferSize(RECEIVE_SAMPLE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
    AudioRecord recorder;
    AudioTrack audioOut;

    public SendAudio() {
        recorder = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                RECEIVE_SAMPLE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, NUM_SAMPLES * 2);

        audioOut = new AudioTrack( AudioManager.STREAM_MUSIC, SAMPLE_RATE_IN_HZ,
                AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
                BUFFER_SIZE_IN_BYTES, AudioTrack.MODE_STREAM);
    }

    public void startTransmission() {
        recorder.startRecording();
        audioOut.play();
        isRecording = true;
        transmissionThread = new Thread(new Runnable()
            {
            @Override
            public void run() {
                transmit();

            }
        }, "AudioRecorder Thread");

        transmissionThread.start();
    }

    private void transmit() {
        short[] buf = new short[NUM_SAMPLES];
        Arrays.fill(buf, (short) 0);
        genAudioTag(buf, 50);
        audioOut.write(buf, 0, buf.length);
        while(isRecording) {
            recorder.read(buf, 0, NUM_SAMPLES);
            audioOut.write(buf, 0, buf.length);
            System.out.println("hi");
        }
    }

    //audio tag wil be 00000001
    private void genAudioTag(short[] inputData, int start) {
        for(int j = 0; j < 7; j++) {//both digital and audio tags have 000,
            inputData[(start * 8 + j) * 4] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 1] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 2] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 3] = (short) 0x8000;
        }
        for(int j = 7; j < 8; j++) {//both digital and audio tags have 000
            inputData[(start * 8 + j) * 4] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 1] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 2] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 3] = (short) 0x7FFF;
        }
    }

    public void talkPressed() {
        if(isRecording) {
            isRecording = false;
        }
        else {
            startTransmission();
        }
    }



}
