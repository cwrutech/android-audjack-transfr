package cwru.com.phonejackinterface;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.BitSet;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.util.Log;

public class OutSignalService extends IntentService {

    public static String LOGTAG = "cwru.com.phonejackinterface";
    private static int SAMPLE_RATE_IN_HZ        = 48000;
    private static int BUFFER_SIZE_IN_BYTES     = 4096;
    private static int BUFFER_SIZE_INPUT        = 1024;
    private static int BITS_TO_TAKE             = 2;
    private static byte[] _buffer               = null;
    private static TestPlatform UIActivity      = null;

    byte[] _inputData = null;
    boolean mPleaseStop = false;
    Context context;
    AudioManager am;

    public OutSignalService() {
        super("OutSignalService");
    }

    public void setUIActivity(TestPlatform UIActivity){
        this.UIActivity = UIActivity;
    }

    public void setSampleRateInHz(int sampleRateInHz){
        SAMPLE_RATE_IN_HZ = sampleRateInHz;
    }

    public void setInputData(byte[] inputData) {
        _inputData = inputData;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOGTAG, "Got to destroy, asking background thread to stop as well");
        mPleaseStop = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = UIActivity.getApplicationContext();
        am = (AudioManager) this.context.getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        boolean doneSending = false;

        Bundle extras = intent.getExtras();
        if (intent != null && extras != null) {

            Log.d(LOGTAG, "Service started: " + extras.getString("ID"));
            setInputData(extras.getByteArray("inputData"));

            if(_inputData.length > BUFFER_SIZE_INPUT){
                Log.e(LOGTAG, "The buffer size limit of " + _inputData + " was exceeded");
                return;
            }
            am = (AudioManager) this.context.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.STREAM_MUSIC);

            am.setSpeakerphoneOn(false);
            AudioTrack myAudioTrack = new AudioTrack( AudioManager.STREAM_MUSIC, SAMPLE_RATE_IN_HZ,
                                            AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT,
                                            BUFFER_SIZE_IN_BYTES, AudioTrack.MODE_STREAM);

            myAudioTrack.play();
            short[] data = manchester(_inputData);
            short[] control;
            if (this.UIActivity.getRBindex() == 0){
                control = sendSignal(1);
            } else {
                control = sendSignal(-1);
            }

            short[] stereo_out = interweaveShorts(control,data);//L,R

            //short[] sound = test_manchester();
            // play message
            myAudioTrack.write(stereo_out, 0, stereo_out.length);

            myAudioTrack.stop();
            myAudioTrack.release();
            am.setSpeakerphoneOn(true);
        } else {
            Log.e(LOGTAG, "The service did not receive enough parameters");
        }

    }

    /**
     * Used to compose the signal message for the hardware
     * @param type here we denote 1 as a high, 0 as a low
     * @return the short array of a burst signal for hardware
     */
    private short[] sendSignal(int type){
        short[] sig = new short[10];
        for (int i = 4; i < sig.length; i++) {
            if(type == 1){
                sig[i] = (short) 0x7FFF;
            }
            if (type == -1){
                sig[i] = (short) 0x8000;
            }
        }
        return sig;
    }

    /**
     * Used to interweave two short arrays
     * @param a short array 1, the first on the combined array
     * @param b shrt array 2
     * @return The combination, interweaved, of a and b with a leading
     */
    private short[] interweaveShorts(short[] a, short[] b){
        int maxSize = a.length > b.length ? a.length : b.length;
        short[] shortOut = new short[maxSize*2];
        for (int i = 0; i < maxSize * 2; i++){
            if (i%2 == 0 && i/2 < a.length){
                shortOut[i] = a[i/2];
            }
            if (i%2 == 1 && i/2 < b.length){
                shortOut[i] = b[i/2];
            }
        }
        return shortOut;
    }

    /**
     * Used to take a certain number of bits from each byte and create a new array that contains those in the MSB.
     * TODO: get all cases... for testing just did 4 bits (simple)
     * @param inputData     Data to be analyzed
     * @return              A byte array that contains the encoded new bytes
     */
    private short[] getSoundArray(byte[] inputData) {

        short[] outputData = new short[inputData.length * 8];

        for(int i = 0; i < inputData.length; i++){
            for(int j = 0; j < 4; j++){
                if((inputData[i] << (2 * j) & 0xC0) == 0xC0){        //if 11
                    outputData[i * 8 + j * 2]     = (short)(0xE000);       //1110 0000 0000 0000
                    outputData[i * 8 + j * 2 + 1] = (short)(0x2000);       //0010 0000 0000 0000
                } else if((inputData[i] << (2 * j) & 0x80) == 0x80){ //if 10
                    outputData[i * 8 + j * 2]     = (short)(0xC000);       //1100 0000 0000 0000
                    outputData[i * 8 + j * 2 + 1] = (short)(0x4000);       //0100 0000 0000 0000
                } else if((inputData[i] << (2 * j) & 0x40) == 0x40){ //if 01
                    outputData[i * 8 + j * 2]     = (short)(0xA000);       //1010 0000 0000 0000
                    outputData[i * 8 + j * 2 + 1] = (short)(0x6000);       //0110 0000 0000 0000
                } else {                                                   //if 00
                    outputData[i * 8 + j * 2]     = (short)(0x8000);       //1000 0000 0000 0000
                    outputData[i * 8 + j * 2 + 1] = (short)(0x7FFF);       //0111 1111 1111 1111
                }
            }
        }
        return outputData;
    }

    private short[] generateSinusoid(int freq) {
        short[] outputData = new short[100000];
        int sinusoid = 0; //for test 2
        for(int i = 0; i < 100000; i++) {
            //Alternate peak voltages on every sample
            //should be expecting sinusoid of frequency 22khz amplitude 150mv
            /*if(i % 2 == 1) {
//                outputData[i] = (short) 0x7FFF;
//            }
//            else {
//                outputData[i] = (short) 0x8000;
            }*/
            //test 2: output smoothed sinusoid (actually a triangle wave)
            outputData[i] = (short)sinusoid;
            if(i % 4000 < 2000) {
                sinusoid = sinusoid + 20;
            } else {
               sinusoid = sinusoid - 20;
            }


            //test 3: sin
            outputData[i] = (short) ((float) Math.sin( (float)i * ((float)(2*Math.PI) * freq / 48000)) * Short.MAX_VALUE);

            //
            //outputData[i] = (short)((i % 3) * 20000);
            //outputData[i] = (short) 32000;
        }
        return outputData;
    }

    //digital tag will be 00010000
    private void genDigitalTag(short[] inputData, int start) {
        for(int j = 0; j < 3; j++) {//both digital and audio tags have 000,
            inputData[(start * 8 + j) * 4] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 1] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 2] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 3] = (short) 0x8000;
        }
        for(int j = 3; j < 8; j++) {//both digital and audio tags have 000
            inputData[(start * 8 + j) * 4] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 1] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 2] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 3] = (short) 0x7FFF;
        }
    }

    private short[] manchester(byte[] inputData) {//convert incoming message to manchester
        int offset = 1000;
        short[] outputData = new short[inputData.length * 4 + 100000];
        Arrays.fill(outputData, (short) 0);

        //Give initial start sequence
        //start sequence is 8 rounds of high high low low
        //genDigitalTag(outputData, offset - 1);

        for(int i = offset; i < inputData.length + offset; i++){
            for(int j = 0; j < 8; j++) {
                if ((inputData[i - offset] & (0x01 << j)) != 0) {
                    outputData[((i) * 8 + j) * 4] = (short) 0x7FFF;
                    outputData[((i) * 8 + j) * 4 + 1] = (short) 0x7FFF;
                    outputData[((i) * 8 + j) * 4 + 2] = (short) 0x8000;
                    outputData[((i) * 8 + j) * 4 + 3] = (short) 0x8000;
                } else {
                    outputData[((i) * 8 + j) * 4] = (short) 0x8000;
                    outputData[((i) * 8 + j) * 4 + 1] = (short) 0x8000;
                    outputData[((i) * 8 + j) * 4 + 2] = (short) 0x7FFF;
                    outputData[((i) * 8 + j) * 4 + 3] = (short) 0x7FFF;
                }
            }
        }

        //Give end sequence
//        for(int i = offset + inputData.length; i < 4 + inputData.length + offset; i++){
//            for(int j = 0; j < 8; j++) {
//                outputData[((i - offset) * 8 + j) * 4] = (short) 0x8000;
//                outputData[((i - offset) * 8 + j) * 4 + 1] = (short) 0x8000;
//                outputData[((i - offset) * 8 + j) * 4 + 2] = (short) 0x7FFF;
//                outputData[((i - offset) * 8 + j) * 4 + 3] = (short) 0x7FFF;
//            }
//        }

        return outputData;
    }

    private short[] test_manchester() {
        short[] outputData = new short[100000];
        boolean longPulse = false;
        for(int i = 0; i < outputData.length; i++) {
            //if (i % 100 == 0) {
              //  longPulse = !longPulse;
            //}
            if (longPulse) {
                if (i % 10 < 4) {
                    outputData[i] = (short) 0x7FFF;
                } else {
                    outputData[i] = (short) 0x8000;
                }
            } else {
                if (i % 4 < 1) {
                    outputData[i] = (short) 0x7FFF;
                } else {
                    outputData[i] = (short) 0x8000;
                }
            }
        }
        return outputData;
    }
}

