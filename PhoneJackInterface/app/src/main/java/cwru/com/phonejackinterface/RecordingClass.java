package cwru.com.phonejackinterface;
import android.content.Context;
import android.hardware.Camera;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ShortBuffer;
import java.util.BitSet;

/**
 * Created by jimmy1 on 2/17/2016.
 */


public class RecordingClass {

    private static short HIGH_LIMIT             = (short)20000;
    private static short LOW_LIMIT              = (short)-20000;

    private static String NO_MESSAGE            = "no meessage found";

    private static boolean MANCHESTER_HIGH      = true;
    private static boolean MANCHESTER_LOW       = false;
    private static boolean ONE_BIT              = true;
    private static boolean ZERO_BIT             = false;

    private short[] lastShorts                  = null;
    private int     index                       = 0;
    private int bufSize = 600000;
    private ShortBuffer voiceInBuf = ShortBuffer.allocate(bufSize);
    private boolean voiceBufUsed = false;
    int RECEIVE_SAMPLE = 48000;
    Thread recordingThread;
    boolean isRecording;
    int NUM_SAMPLES;//AudioRecord.getMinBufferSize(RECEIVE_SAMPLE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
    AudioRecord recorder;
    private Context context;
    protected TestPlatform UIActivity;
    AudioTrack audioOut = new AudioTrack( AudioManager.STREAM_MUSIC, RECEIVE_SAMPLE,
                               AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
                               4096, AudioTrack.MODE_STREAM);
    AudioManager am;

    int recordMode = 0; //indicates expectation of voice or digital transmission, 0 for digital, 1 for voice
    boolean talking = false;

    public RecordingClass(TestPlatform UIActivity, int mode) {
        makeAudioRecord(mode);

        this.UIActivity = UIActivity;
        this.context = UIActivity.getApplicationContext();
        am = (AudioManager) this.context.getSystemService(Context.AUDIO_SERVICE);
        am.setMode(AudioManager.STREAM_MUSIC);
        am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        am.setSpeakerphoneOn(true);
        System.out.println("audio set to speaker");

    }

    public void makeAudioRecord(int mode) {
        if(recorder != null) {
            recorder.release();
        }
        if(mode == 0) {
            NUM_SAMPLES = 100000;
        }
        else {
            NUM_SAMPLES = 2048;
        }
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECEIVE_SAMPLE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, NUM_SAMPLES * 2);
    }
    public void startRecordingVoice() {
        //recorder.startRecording();
        System.out.println("crash happend after startrecord");
        isRecording = true;
        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {
                getVoiceToBuf();
            }
        }, "AudioRecorder Thread");

        recordingThread.start();
    }
    public void startRecording() {
        recorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {
                writeAudioDataToFile();

            }
        }, "AudioRecorder Thread");

        recordingThread.start();
    }

    public void stopRecording() {
        isRecording = false;
    }

    /**
     * Print a message to the screen
     * @param msg The message to be printed
     */
    public void displayMsg( final String msg){
        UIActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                UIActivity._MsgRcv.setText(msg);
            }
        });
    }

    public void getVoiceToBuf() {
        recorder.startRecording();
        short[] sData = new short[NUM_SAMPLES];
        System.out.println("Ready to get voice from user" + voiceInBuf.capacity());
        int iBufferReadResult;
        int iBytesRead = 0;
        while (talking) {
            // gets the voice output from microphone to byte format
            iBufferReadResult = recorder.read(sData, 0, NUM_SAMPLES);
            // Android is reading less number of bytes than requested.
            if(NUM_SAMPLES > iBufferReadResult)
            {
                iBufferReadResult = iBufferReadResult +
                        recorder.read(sData, iBufferReadResult - 1, NUM_SAMPLES - iBufferReadResult);
            }
            iBytesRead = iBytesRead + iBufferReadResult;

            System.out.println("writing voice msg to buffer to play");
            setBufPercent();
            if(voiceInBuf.position() < voiceInBuf.capacity() - NUM_SAMPLES) {
                voiceInBuf.put(sData, 0, iBufferReadResult);
            }
            else {
                System.out.println("buffer overflowed");
                setTalk(false);
            }
        }
        System.out.println("buf fin at: " + voiceInBuf.position());
        recorder.stop();
        //recorder.release();
        voiceBufUsed = true;
    }
    private void setBufPercent() {
        int bufPos = voiceInBuf.position();
        final double progress = ((double) bufPos)/bufSize * 100;
        UIActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UIActivity.setTalkProgress((int) progress);
            }
        });

    }

    //1 byte 1110000
    private void getActualAnalogTag(short[] inputData, int start){
        for(int j = 0; j < 8; j++) {
            inputData[(start * 8 + j) * 4] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 1] = (short) 0x7FFF;
            inputData[(start * 8 + j) * 4 + 2] = (short) 0x8000;
            inputData[(start * 8 + j) * 4 + 3] = (short) 0x8000;
        }
    }

    //digital tag will be 00010000
    private short[] genAnalogTag() {
        short[] inputData = new short[32000];
        getActualAnalogTag(inputData, 100);
        getActualAnalogTag(inputData, 247);
        getActualAnalogTag(inputData, 368);
        getActualAnalogTag(inputData, 485);
        getActualAnalogTag(inputData, 500);

        return inputData;
    }


    public void writeBufToAudio() {
        voiceBufUsed = false;
        am.setSpeakerphoneOn(false);
        audioOut.play();
        short[] tag = genAnalogTag();
        audioOut.write(tag, 0, tag.length);
        short[] arr = voiceInBuf.array();
        System.out.println(arr.length);
        //todo: block send and update progress
        audioOut.write(arr, 0, arr.length);
        audioOut.stop();
        am.setSpeakerphoneOn(true);
        voiceInBuf = ShortBuffer.allocate(bufSize);
        //voiceInBuf.clear();

        //System.out.println("voiceBufUsed got called shit");

    }

    public boolean voiceBufUsed() {
        return voiceBufUsed;
    };

    private void writeAudioDataToFile() {
        // Write the output audio in byte
        if(recordMode == 1) {
            audioOut.play();
        }
        File filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
	    File newFile = new File(filePath + "/recordeddata.pcm");
        short sData[] = new short[NUM_SAMPLES];

        FileOutputStream os = null;
        DataOutputStream ds = null;
        FileWriter fw = null;
        try {
            os = new FileOutputStream(newFile);
            ds = new DataOutputStream(os);
            fw = new FileWriter(newFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Waiting for receiving messages");
//            while (timeout > 0 && recorder.getState() != AudioRecord.STATE_INITIALIZED) {
//                Thread.sleep(50);
//                timeout -= 50;
//            }
//        } catch (InterruptedException e) { }
        //recorder.startRecording();
        am.setSpeakerphoneOn(true);


        while (isRecording) {
            // gets the voice output from microphone to byte format
            //System.out.println("we recording");
            recorder.read(sData, 0, NUM_SAMPLES);

            if(recordMode == 1) {
//                am.setMode(AudioManager.STREAM_MUSIC);
//                am.setSpeakerphoneOn(true);
                System.out.println("playing received voice data");
                audioOut.write(sData, 0, NUM_SAMPLES);
            }
//            else if(mymessage.charAt(0) == 1) {//This is a voice transmission
//                am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//                am.setMode(AudioManager.STREAM_MUSIC);
//                am.setSpeakerphoneOn(true);
//                recordMode = 1;
//                audioOut.play();
//                audioOut.write(sData, 0, NUM_SAMPLES);
//                System.out.println("got a voice transmission");
//            }
            else {
                byte[] mymessageBytes = decodeCharArrSetI(0, sData.length - 1, sData);
                //System.out.println(mymessage);
                if(!decodeToString(mymessageBytes).equals(NO_MESSAGE)) {
                    if (mymessageBytes[0] == 0xFF){
                        recordMode = 1;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(15000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                recordMode = 0;
                            }
                        }, "Timer Thread").start();
                    }
                    displayMsg(decodeToString(mymessageBytes));
                }

            }
//            try {
//
//                // // writes the data to file from buffer
//                // // stores the voice buffer
//                //byte bData[] = short2byte(sData);
//
//                for(int i = 0; i < sData.length; i++) {
//                    //ds.write(sData[i]);
//
//                    fw.write(sData[i] + "\r\n");
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
//        try {
//           os.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        recorder.stop();
//        recorder.release();
    }


    /**
     * Used to interweave two short arrays
     * @param a short array 1, the first on the combined array
     * @param b shrt array 2
     * @return The combination, interweaved, of a and b with a leading
     */
    private short[] interweaveShorts(short[] a, short[] b){
        int maxSize = a.length > b.length ? a.length : b.length;
        short[] shortOut = new short[maxSize*2];
        for (int i = 0; i < maxSize * 2; i++){
            if (i%2 == 0 && i/2 < a.length){
                shortOut[i] = a[i/2];
            }
            if (i%2 == 1 && i/2 < b.length){
                shortOut[i] = b[i/2];
            }
        }
        return shortOut;
    }


    //assume channel1 is the audio jack mic, channel2 is the camcorder mic, need to test this to confirm
    private void separateChannels(short[] input, short[] channel1, short[] channel2) {
        for(int i = 0; i < input.length; i = i + 2) {
            channel1[i/2] = input[i];
            channel2[i/2] = input[i + 1];
        }
    }
    public void endRecord() {
        recorder.stop();
        recorder.release();
        isRecording = false;
    }
    public void setTalk(boolean talk) {
        talking = talk;
        System.out.println("switched to " + talk + "talk mode");
    }

    public String decodeToString(byte[] arr) {
        try {

            String str = new String(arr, "ASCII");
            if(!str.equals(NO_MESSAGE)){
                for(int i= 0; i<arr.length; i++){
                    System.out.println(arr[i]);
                }
            }
            return new String(arr, "ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * Wrapper for decodeCharArr with the index for the byte bit (in little endian) being set to 0
     */
    public byte[] decodeCharArrSetI(int begin, int end, short[] shortArr){
        index = 0;
        return decodeCharArr(begin, end, shortArr);
    }

    /**
     * Uses the buffer char[] and returns the byte[] decoding of it
     * This method assumes only a single transmission
     * @param begin The starting position in the shortArr
     * @param end The end position in the shortArr
     * @param shortArr The array containing the manchester code
     * @return A byte array of the short[] encoded manchester. If it was chopped it is
     * the responsability of the caller to unify byte[] properly (even though this method
     * supports chopped messages. aka it remembers the needed states for computation)
     */
    private byte[] decodeCharArr(int begin, int end, short[] shortArr){
        int     startPos    = 0;
        int     endPos      = 0;


        BitSet myBits = new BitSet();
        try{
            if(lastShorts != null){
                //have to check the last transmission
                if(shortArr.length >= (4-lastShorts.length)){//TODO: needed?
                    if(lastShorts.length == 1){
                        setMachesterQuadubpleDecode(myBits,index, lastShorts[0], shortArr[0],
                                shortArr[1], shortArr[2]);
                        startPos = 3;
                    } else if(lastShorts.length == 2){
                        setMachesterQuadubpleDecode(myBits,index, lastShorts[0], lastShorts[1],
                                shortArr[0], shortArr[1]);
                        startPos = 2;
                    } else{
                        setMachesterQuadubpleDecode(myBits,index,lastShorts[0],lastShorts[1],
                                lastShorts[2],shortArr[0]);
                        startPos = 1;
                    }
                    lastShorts = null;
                    index++;
                }

            }

            //Get index to start from, be sure to check if you already set it because of the last transmission!!
            //Additionally check if we can we started on this array
            if(!(startPos > 0) && (startPos = getStartPoss(begin,shortArr,end))== -1){
                String s = NO_MESSAGE;
                byte[] b = new byte[100];
                try {
                    b = s.getBytes("ASCII");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return b;
            }

            endPos = getEndPoss(startPos,shortArr,end);
            System.out.println("start pos is: " + startPos);
            System.out.println("end pos is: " + endPos);

            if (shortArr.length - begin >= startPos + 4){
                for(;startPos + 3 <= endPos;startPos = startPos + 4){
                    setMachesterQuadubpleDecode(myBits,index, shortArr[startPos], shortArr[startPos + 1],
                            shortArr[startPos + 2], shortArr[startPos + 3]);
                    index++;
                }
            }

            //Save the last shorts if the transmission is cut
            if(startPos <= endPos){
                lastShorts = new short[endPos - startPos + 1];
                for(int i = startPos; i <= endPos;i++){
                    lastShorts[i-startPos] = shortArr[i];
                }
            }
        } catch(IndexOutOfBoundsException e) {
            String s = NO_MESSAGE;
            byte[] b = new byte[100];
            try {
                b = s.getBytes("ASCII");
            } catch (UnsupportedEncodingException e2) {
                e.printStackTrace();
            }
            return b;
        }

        index = index % 8;
        return myBits.toByteArray();
    }

    /**
     * Used to obtain the start of the stream as identified by a spike in the possitive and negative direction
     * @param begin The start of the array
     * @param arr The buffered array
     * @param end The end of the array
     * @return The start of the predicted manchester period
     */
    private int getStartPoss(int begin, short[] arr, int end){
        if(end-begin < 3){
            return begin;
        }
        for(int current = begin; current < arr.length; current++){
            if(arr[current] <= LOW_LIMIT){
                if((arr[current + 1] > LOW_LIMIT && arr[current + 1] < HIGH_LIMIT && arr[current + 2] >= HIGH_LIMIT) ||
                        (arr[current + 1] <= LOW_LIMIT && arr[current + 2] > LOW_LIMIT && arr[current + 2] < HIGH_LIMIT && arr[current + 3] >= HIGH_LIMIT) ||
                        (arr[current + 1] <= LOW_LIMIT && arr[current + 2] >= HIGH_LIMIT)){
                    return current;
                }
            } else if(arr[current] >= HIGH_LIMIT){
                if((arr[current + 1] > LOW_LIMIT && arr[current + 1] < HIGH_LIMIT && arr[current + 2] <= LOW_LIMIT) ||
                        (arr[current + 1] >= HIGH_LIMIT && arr[current + 2] > LOW_LIMIT && arr[current + 2] < HIGH_LIMIT && arr[current + 3] <= LOW_LIMIT) ||
                        (arr[current + 1] >= HIGH_LIMIT && arr[current + 2] <= LOW_LIMIT)){
                    return current;
                }
            }
        }
        return -1;
    }

    /**
     * Used to obtain the end of the manchester encoding in the array
     * @param begin The start of the array
     * @param arr The buffered array
     * @param end The end of the array
     * @return The predicted end value
     */
    private int getEndPoss(int begin, short[] arr, int end){
        if(end-begin < 2){
            return end;
        }
        for(int current = begin; current < arr.length; current++){
            if(arr[current] > LOW_LIMIT && arr[current] < HIGH_LIMIT && current < end &&
                    (arr[current + 1] > LOW_LIMIT && arr[current + 1] < HIGH_LIMIT)){
                if((current - begin) % 2 == 0){
                    return current - 1;
                } else {
                    return current;
                }
            }
        }
        return end;
    }

    /**
     * Takes four chars and determines the binary value in manchester then adds
     * The fancy math is to turn the index to Big Endian(ex.2->5,10->13)
     * (7+8*(index/8)-index%8)
     * @param bits The bitset to add our value to
     * @param A The first char in group with a
     * @param a A's compliment bit
     * @param B The third char in group with b
     * @param b B's coompliment bit
     */
    private void setMachesterQuadubpleDecode(BitSet bits, int index, short A, short a, short B,short b) throws IndexOutOfBoundsException{
        try{
            if(getLevel(A, a) &&
                    !getLevel(B, b)){
                bits.set(index, ONE_BIT);
            } else if(!getLevel(A, a) &&
                    getLevel(B, b)){
                bits.set(index, ZERO_BIT);
            }
        } catch (Exception e){
            throw new IndexOutOfBoundsException();
        }

    }

    /**
     * Checks the status of a and b for states H or L in manchester with the limit threshold
     * @param a First char to analyze
     * @param b Second char to analyze
     * @return The state of chars a and b
     */
    private Boolean getLevel(short a,short b){
        if(a >= HIGH_LIMIT && b >= HIGH_LIMIT ||
                a >= HIGH_LIMIT && b < HIGH_LIMIT && b > LOW_LIMIT ||
                b >= HIGH_LIMIT && a < HIGH_LIMIT && a > LOW_LIMIT){
            return MANCHESTER_HIGH;
        } else if(a <= LOW_LIMIT && b <= LOW_LIMIT ||
                a <= LOW_LIMIT && b > LOW_LIMIT && b < HIGH_LIMIT ||
                b <= LOW_LIMIT && a > LOW_LIMIT && a < HIGH_LIMIT){
            return MANCHESTER_LOW;
        }
        return null;
    }
}