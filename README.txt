https://youtu.be/1rwIbOHNumM

Demo can be found at that URL. Note that the audio play cuts off and then comes back. This is simply a result of the connector getting slightly disconnected during transmission.